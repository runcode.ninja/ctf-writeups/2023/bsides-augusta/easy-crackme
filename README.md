Easy Crackme

Opening up the provided binary in ghidra and looking at the main function
![image info](./images/1.png)

We can see a `strcmp` right before it prints out if the password was correct or not.

Since [strcmp](https://www.programiz.com/c-programming/library-function/string.h/strcmp) takes 2 strings and returns a `0` if they are the same and something other than `0` if differnt, we can assume that our password (supplied by the `fgets`) is compared against the vaild password.

Lets set a breakpoint in gdb at the call to `strcmp` and check what is being passed to it.

![image info](./images/2.png)

if we `c`ontinue execution we will be prompted to enter a password, once we input a value the program will continue until out break point is hit.

![image info](./images/3.png)

Currently I have a GDB extenstion loaded called [PEDA](https://github.com/longld/peda), PEDA does a good job at guessing which registers/stack address's are going to be used as arguments to calls. In this case we can see that in the guessed arguments one is our input and another is the flag.